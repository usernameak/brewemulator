#include "breJni.h"

#include <pthread.h>

JavaVM *gJVM = NULL;

static int jniEnvTssInitialized = 0;
static pthread_key_t jniEnvTssKey = 0;

static void jniEnvDtor(void *p) {
    (*gJVM)->DetachCurrentThread(gJVM);
}

JNIEnv *breGetThreadLocalJNIEnv() {
    if (!jniEnvTssInitialized) {
        if (0 != pthread_key_create(&jniEnvTssKey, jniEnvDtor)) return NULL;
        jniEnvTssInitialized = 1;
    }
    JNIEnv *env = (JNIEnv *) pthread_getspecific(jniEnvTssKey);
    if (!env) {
        if (JNI_OK == (*gJVM)->AttachCurrentThread(gJVM, &env, NULL)) {
            pthread_setspecific(jniEnvTssKey, env);
        } else {
            env = NULL;
        }
    }
    return env;
}
