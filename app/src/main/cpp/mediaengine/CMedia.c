#include "CMedia.h"

#include <AEEInterface.h>
#include <AEEMedia.h>
#include <AEEStdLib.h>

#include <ktmeAudioDecoder.h>
#include <ktmeChannel.h>
#include <ktmeMixer.h>
#ifdef __ANDROID__
#include <android/log.h>
#endif

typedef struct CMediaEngine {
    ktmeEngine *m_ktmeEngine;
    ktmeMixer *m_mixer;
} CMediaEngine;

static void sysFuncLogMessage(const char *format, va_list args) {
#ifdef __ANDROID__
    __android_log_vprint(ANDROID_LOG_INFO, "BREWEmulator_MediaEngine", format, args);
#else
    vfprintf(stderr, format, args);
#endif
}

static CMediaEngine *CMediaEngine_New() {
    CMediaEngine *self = malloc(sizeof(CMediaEngine));
    ktmeEngineSysFuncs sysFuncs = {};
    sysFuncs.memAlloc = malloc;
    sysFuncs.memFree = free;
    sysFuncs.logMessage = sysFuncLogMessage;

    self->m_ktmeEngine = ktmeEngineCreate(&sysFuncs);
    self->m_mixer = ktmeMixerCreate(self->m_ktmeEngine, 8);
    return self;
}

static void CMediaEngine_Destroy(CMediaEngine *self) {
    ktmeMixerDestroy(self->m_mixer);
    ktmeEngineDestroy(self->m_ktmeEngine);
}

typedef struct CMedia {
    const AEEVTBL(IMedia) *pvt;
    uint32 m_nRefs;

    ktmeEngine *m_engine;
    ktmeChannel *m_channel;
    ktmeAudioSourceBase *m_audioSource;
    ktmeDataSource *m_dataSource;

    PFNMEDIANOTIFY m_notifyCallback;
    void *m_notifyCallbackUser;
} CMedia;

static uint32 CMedia_AddRef(IMedia *po) {
    CMedia *pMe = (CMedia *) po;

    return (++(pMe->m_nRefs));
}

static uint32 CMedia_Release(IMedia *po) {
    CMedia *pMe = (CMedia *) po;
    if (pMe->m_nRefs) {
        if (--pMe->m_nRefs == 0) {
            FREE(po);
        }
    }
    return pMe->m_nRefs;
}

static int CMedia_QueryInterface(IMedia *po, AEEIID cls, void **ppo) {
    switch (cls) {
        case AEECLSID_QUERYINTERFACE:
        case AEECLSID_MEDIA:
            *ppo = (void *) po;
            CMedia_AddRef(po);
            return SUCCESS;
        default:
            *ppo = NULL;
            return ECLASSNOTSUPPORT;
    }
}

int CMedia_RegisterNotify(IMedia *po, PFNMEDIANOTIFY pfnNotify, void *pUser) {
    CMedia *pMe = (CMedia *) po;

    pMe->m_notifyCallback = pfnNotify;
    pMe->m_notifyCallbackUser = pUser;

    return SUCCESS;
}

int CMedia_SetMediaParm(IMedia *po, int nParamID, int32 p1, int32 p2) {
    return EUNSUPPORTED;
}

int CMedia_GetMediaParm(IMedia *po, int nParamID, int32 *pP1, int32 *pP2) {
    return EUNSUPPORTED;
}

int CMedia_Play(IMedia *po) {
    return EUNSUPPORTED;
}

int CMedia_Record(IMedia *po) {
    return EUNSUPPORTED;
}

int CMedia_Stop(IMedia *po) {
    return EUNSUPPORTED;
}

int CMedia_Seek(IMedia *po, AEEMediaSeek eSeek, int32 lSeekValue) {
    return EUNSUPPORTED;
}

int CMedia_Pause(IMedia *po) {
    return EUNSUPPORTED;
}

int CMedia_Resume(IMedia *po) {
    return EUNSUPPORTED;
}

int CMedia_GetTotalTime(IMedia *po) {
    return EUNSUPPORTED;
}

int CMedia_GetState(IMedia *po, boolean *pbStateChanging) {
    if (*pbStateChanging) *pbStateChanging = FALSE;

    return MM_STATE_IDLE;
}

static const VTBL(IMedia) gsCMediaFuncs = {
        CMedia_AddRef,
        CMedia_Release,
        CMedia_QueryInterface,
        CMedia_RegisterNotify,
        CMedia_SetMediaParm,
        CMedia_GetMediaParm,
        CMedia_Play,
        CMedia_Record,
        CMedia_Stop,
        CMedia_Seek,
        CMedia_Pause,
        CMedia_Resume,
        CMedia_GetTotalTime,
        CMedia_GetState,
};

int CMedia_New(IShell *ps, AEECLSID ClsId, void **ppObj) {
    CMedia *pNew;

    *ppObj = NULL;

    if (ClsId == AEECLSID_MEDIA ||
        ClsId == AEECLSID_MEDIAADPCM) {

        pNew = (CMedia *) MALLOC(sizeof(CMedia));
        if (!pNew) {
            return ENOMEMORY;
        } else {
            pNew->pvt = &gsCMediaFuncs;
            // pNew->m_engine = acquireKtmeEngine();
            // pNew->m_channel =
            pNew->m_nRefs = 1;

            *ppObj = pNew;
            return AEE_SUCCESS;
        }
    }

    return EUNSUPPORTED;
}

