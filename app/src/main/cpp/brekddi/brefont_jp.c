#include <AEE_OEM.h>
#include <OEMHeap.h>
#include <AEEFile.h>
#include "AEEFont.h"
#include "AEEDisp.h"
#include "AEEStdLib.h"

static uint32 CFontJP_AddRef(IFont *po);

static uint32 CFontJP_Release(IFont *po);

static int CFontJP_QueryInterface(IFont *po, AEECLSID id, void **ppif);

static int CFontJP_DrawText(IFont *pMe, IBitmap *pDst, int x, int y,
                            const AECHAR *pcText, int nChars,
                            NativeColor clrFg, NativeColor clrBg,
                            const AEERect *prcClip, uint32 dwFlags);

static int CFontJP_MeasureText(IFont *pMe, const AECHAR *pcText, int nChars,
                               int nMaxWidth, int *pnFits, int *pnPixels);

static int CFontJP_GetInfo(IFont *pMe, AEEFontInfo *pInfo, int nSize);


static const AEEVTBL(IFont) gKanjiFontFuncs =
        {CFontJP_AddRef,
         CFontJP_Release,
         CFontJP_QueryInterface,
         CFontJP_DrawText,
         CFontJP_MeasureText,
         CFontJP_GetInfo};

typedef struct {
    uint32 index;
    uint32 encoding;
    uint32 bboxWidth;
    uint32 bboxHeight;
    uint32 dwidthX;
    uint32 dwidthY;
} FontChar;

typedef struct CFont {
    const AEEVTBL(IFont) *pvt;
    uint32 m_nRefs;
    IShell *m_pIShell;
    IDIB *m_fontBmp;
    uint32 m_bboxWidth;
    uint32 m_bboxWidthAligned;
    uint32 m_bboxHeight;
    uint32 m_numChars;
    uint32 m_ascent;
    uint32 m_descent;
    FontChar *m_fontChars; // open-addressed hashtable
} CFont;

static uint32 CFontJP_AddRef(IFont *pme) {
    CFont *pMe = (CFont *) pme;

    return (++(pMe->m_nRefs));
}

static uint32 CFontJP_Release(IFont *po) {
    CFont *pMe = (CFont *) po;
    if (pMe->m_nRefs) {
        if (--pMe->m_nRefs == 0) {
            if (pMe->m_fontChars) FREE(pMe->m_fontChars);
            if (pMe->m_fontBmp) IDIB_Release(pMe->m_fontBmp);
            sys_free(po);
        }
    }
    return pMe->m_nRefs;
}

static int CFontJP_QueryInterface(IFont *pMe, AEECLSID id, void **pvtbl) {
    void *po = 0;

    if (id == AEECLSID_FONT) {
        IFONT_AddRef(pMe);
        po = (void *) pMe;
    }

    *pvtbl = po;
    return (po != 0 ? SUCCESS : ECLASSNOTSUPPORT);
}

#define RGBVAL_TO_RGB(r)    (((r) << 8) & 0x00ff0000 | ((r) >> 8) & 0x0000ff00 | ((r) >> 24) & 0x000000ff)

static FontChar *CFontJP_FindChar(CFont *font, uint32 encoding) {
    uint32 hash = encoding;
    for (uint32 i = 0; i < font->m_numChars; i++) {
        uint32 index = hash % font->m_numChars;
        if (font->m_fontChars[index].encoding == encoding) {
            return &font->m_fontChars[index];
        }
        if (font->m_fontChars[index].encoding == 0) {
            return NULL;
        }
        hash++;
    }
    return NULL;
}

static FontChar *CFontJP_FindCharFreeSpace(CFont *font, uint32 encoding) {
    uint32 hash = encoding;
    for (uint32 i = 0; i < font->m_numChars; i++) {
        uint32 index = hash % font->m_numChars;
        if (font->m_fontChars[index].encoding == encoding ||
            font->m_fontChars[index].encoding == 0) {
            return &font->m_fontChars[index];
        }
        hash++;
    }
    return NULL;
}

static void CFontJP_LocateChar(CFont *self, FontChar *fc, uint32 *charX, uint32 *charY) {
    *charX = (fc->index % 32) * self->m_bboxWidthAligned;
    *charY = (fc->index / 32) * self->m_bboxHeight;
}

static int CFontJP_ShiftJISToJIS(unsigned char first, unsigned char second,
                                 unsigned char *jis_first_ptr,
                                 unsigned char *jis_second_ptr) {
    int status = 0;
    int jis_first = 0;
    int jis_second = 0;
    /* Check first byte is valid shift JIS. */
    if ((first >= 0x81 && first <= 0x84) ||
        (first >= 0x87 && first <= 0x9f)) {
        jis_first = 2 * (first - 0x70) - 1;
        if (second >= 0x40 && second <= 0x9e) {
            jis_second = second - 31;
            if (jis_second > 95) {
                jis_second -= 1;
            }
            status = 1;
        } else if (second >= 0x9f && second <= 0xfc) {
            jis_second = second - 126;
            jis_first += 1;
            status = 1;
        }
    } else if (first >= 0xe0 && first <= 0xef) {
        jis_first = 2 * (first - 0xb0) - 1;
        if (second >= 0x40 && second <= 0x9e) {
            jis_second = second - 31;
            if (jis_second > 95) {
                jis_second -= 1;
            }
            status = 1;
        } else if (second >= 0x9f && second <= 0xfc) {
            jis_second = second - 126;
            jis_first += 1;
            status = 1;
        }
    }
    *jis_first_ptr = (unsigned char) jis_first;
    *jis_second_ptr = (unsigned char) jis_second;
    return status;
}

static FontChar *CFontJP_FindCharAECHAR(CFont *font, AECHAR cha) {
    uint16_t jis;
    if (cha >= 0x20 && cha <= 0x7F) {
        jis = cha;
    } else {
        uint32_t uCha = cha;
        unsigned char first = uCha & 0xFFu;
        unsigned char second = uCha >> 8u;
        unsigned char jis_first, jis_second;
        if (CFontJP_ShiftJISToJIS(first, second, &jis_first, &jis_second)) {
            jis = (uint16_t) (((uint16_t) jis_first) << 8u) | jis_second;
        } else {
            jis = 0;
        }
    }

    return CFontJP_FindChar(font, jis);
}

typedef struct {
    uint32 bufferOffset;
    uint32 bufferRemaining;
    uint32 tokenOffset;
    char buffer[256];
    char lineBuffer[128];
} BufferedLineReaderState;

static void BufferedLineReader_Init(BufferedLineReaderState *state) {
    state->bufferOffset = 0;
    state->bufferRemaining = 0;
}

static int
BufferedLineReader_ReadLine(BufferedLineReaderState *state, IFileMgr *fileMgr, IFile *file) {
    state->tokenOffset = 0;
    char *outPtr = state->lineBuffer;
    while (1) {
        while (state->bufferRemaining) {
            char c = state->buffer[state->bufferOffset];
            state->bufferOffset++;
            state->bufferRemaining--;
            if (c == '\n') {
                *outPtr = 0;
                return AEE_SUCCESS;
            }
            *outPtr++ = c;
        }
        int numRead = IFILE_Read(file, state->buffer, sizeof(state->buffer));
        if (numRead == 0) {
            *outPtr = 0;
            int status = IFILEMGR_GetLastError(fileMgr);
            if (status == AEE_SUCCESS) return EFILEEOF;
            return status;
        }
        state->bufferOffset = 0;
        state->bufferRemaining = numRead;
    }
}

static const char *BufferedLineReader_NextToken(BufferedLineReaderState *state) {
    if (state->tokenOffset == UINT32_MAX) return NULL;

    uint32 startTokenOffset = state->tokenOffset;

    for (uint32 i = startTokenOffset; i < 128; i++) {
        if (state->lineBuffer[i] == 0) {
            state->tokenOffset = UINT32_MAX;
            break;
        } else if (state->lineBuffer[i] == ' ') {
            state->tokenOffset = i + 1;
            state->lineBuffer[i] = 0;
            break;
        }
    }
    return &state->lineBuffer[startTokenOffset];
}

#define BDF_STR_CHARS "CHARS"
#define BDF_STR_FONTBOUNDINGBOX "FONTBOUNDINGBOX"
#define BDF_STR_FONT_ASCENT "FONT_ASCENT"
#define BDF_STR_FONT_DESCENT "FONT_DESCENT"
#define BDF_STR_STARTCHAR "STARTCHAR"
#define BDF_STR_ENCODING "ENCODING"
#define BDF_STR_DWIDTH "DWIDTH"
#define BDF_STR_BBX "BBX"
#define BDF_STR_BITMAP "BITMAP"
#define BDF_STR_ENDCHAR "ENDCHAR"

static uint32 CFontJP_HexCharToInt(char hexChar) {
    if (hexChar >= '0' && hexChar <= '9') return hexChar - '0';
    if (hexChar >= 'A' && hexChar <= 'F') return hexChar - 'A' + 0xA;
    if (hexChar >= 'a' && hexChar <= 'f') return hexChar - 'a' + 0xA;
    return 0;
}

static int CFontJP_InitFont(CFont *self) {
    int status = AEE_SUCCESS;
    IFileMgr *pFileMgr = NULL;
    IFile *pFile = NULL;
    BufferedLineReaderState reader;
    IDisplay *pDisplay = NULL;

    status = ISHELL_CreateInstance(self->m_pIShell, AEECLSID_DISPLAY, (void **) &pDisplay);
    if (status != AEE_SUCCESS) return status;

    status = ISHELL_CreateInstance(self->m_pIShell, AEECLSID_FILEMGR, (void **) &pFileMgr);
    if (status != AEE_SUCCESS) {
        IDISPLAY_Release(pDisplay);
        return status;
    }

    pFile = IFILEMGR_OpenFile(pFileMgr, "fs:/sys/fonts/fontjp.bdf", _OFM_READ);
    if (!pFile) {
        IDISPLAY_Release(pDisplay);
        IFILEMGR_Release(pFileMgr);
        return IFILEMGR_GetLastError(pFileMgr);
    }

    BufferedLineReader_Init(&reader);

    FontChar currentChar;
    uint32 nextCharIndex = 0;

    while (1) {
        status = BufferedLineReader_ReadLine(&reader, pFileMgr, pFile);
        if (status == EFILEEOF) break;
        if (status != AEE_SUCCESS) {
            IDISPLAY_Release(pDisplay);
            IFILE_Release(pFile);
            IFILEMGR_Release(pFileMgr);
            return status;
        }

        const char *token = BufferedLineReader_NextToken(&reader);

        if (STRCMP(token, BDF_STR_FONTBOUNDINGBOX) == 0) {
            self->m_bboxWidth = ATOI(BufferedLineReader_NextToken(&reader));
            self->m_bboxWidthAligned = (self->m_bboxWidth + 7u) & ~7u;
            self->m_bboxHeight = ATOI(BufferedLineReader_NextToken(&reader));

        } else if (STRCMP(token, BDF_STR_FONT_ASCENT) == 0) {
            self->m_ascent = ATOI(BufferedLineReader_NextToken(&reader));
        } else if (STRCMP(token, BDF_STR_FONT_DESCENT) == 0) {
            self->m_descent = ATOI(BufferedLineReader_NextToken(&reader));
        } else if (STRCMP(token, BDF_STR_CHARS) == 0) {
            self->m_numChars = ATOI(BufferedLineReader_NextToken(&reader));
            self->m_fontChars = MALLOC(self->m_numChars * sizeof(FontChar));
            uint32 width = 32 * self->m_bboxWidthAligned;
            uint32 height = ((self->m_numChars + 31) / 32) * self->m_bboxHeight;
            status = IDISPLAY_CreateDIBitmapEx(pDisplay, &self->m_fontBmp, 1, height, width, 2, 0);
            if (status != AEE_SUCCESS) {
                IDISPLAY_Release(pDisplay);
                IFILE_Release(pFile);
                IFILEMGR_Release(pFileMgr);
                return status;
            }
        } else if (STRCMP(token, BDF_STR_STARTCHAR) == 0) {
            MEMSET(&currentChar, 0, sizeof(currentChar));
            currentChar.index = nextCharIndex++;
        } else if (STRCMP(token, BDF_STR_ENCODING) == 0) {
            currentChar.encoding = ATOI(BufferedLineReader_NextToken(&reader));
        } else if (STRCMP(token, BDF_STR_DWIDTH) == 0) {
            currentChar.dwidthX = ATOI(BufferedLineReader_NextToken(&reader));
            currentChar.dwidthY = ATOI(BufferedLineReader_NextToken(&reader));
        } else if (STRCMP(token, BDF_STR_BBX) == 0) {
            currentChar.bboxWidth = ATOI(BufferedLineReader_NextToken(&reader));
            currentChar.bboxHeight = ATOI(BufferedLineReader_NextToken(&reader));
        } else if (STRCMP(token, BDF_STR_BITMAP) == 0) {
            uint32 charX = (currentChar.index % 32) * self->m_bboxWidthAligned / 8;
            uint32 charY = (currentChar.index / 32) * self->m_bboxHeight;
            for (int y = 0; y < currentChar.bboxHeight; y++) {
                status = BufferedLineReader_ReadLine(&reader, pFileMgr, pFile);
                if (status == EFILEEOF) break;
                if (status != AEE_SUCCESS) {
                    IFILE_Release(pFile);
                    IFILEMGR_Release(pFileMgr);
                    return status;
                }

                for (int x = 0; x < (currentChar.bboxWidth + 7) / 8; x++) {
                    byte b = (CFontJP_HexCharToInt(reader.lineBuffer[x * 2]) << 4u) |
                            CFontJP_HexCharToInt(reader.lineBuffer[x * 2 + 1]);
                    self->m_fontBmp->pBmp[charX + x + (charY + y) * self->m_fontBmp->cx / 8] = b;
                }
            }
        } else if (STRCMP(token, BDF_STR_ENDCHAR) == 0) {
            MEMCPY(CFontJP_FindCharFreeSpace(self, currentChar.encoding), &currentChar,
                   sizeof(currentChar));
        }
    }

    IDISPLAY_Release(pDisplay);
    IFILE_Release(pFile);
    IFILEMGR_Release(pFileMgr);

    return AEE_SUCCESS;
}

static int
CFontJP_DrawText(IFont *pMe, IBitmap *pDst, int x, int y, const AECHAR *pcText, int nChars,
                 NativeColor foreground, NativeColor background, const AEERect *prcClip,
                 uint32 dwFlags) {
    CFont *self = (CFont *) pMe;

    if (!self->m_fontBmp) {
        int status = CFontJP_InitFont(self);
        if (status != AEE_SUCCESS) {
            return status;
        }
    }

    RGBVAL rgb0 = IBitmap_NativeToRGB(pDst, background);
    RGBVAL rgb1 = IBitmap_NativeToRGB(pDst, foreground);
    uint32 u0 = RGBVAL_TO_RGB(rgb0);
    uint32 u1 = RGBVAL_TO_RGB(rgb1);
    uint32 *pRGB = self->m_fontBmp->pRGB;
    if ((u0 != pRGB[0] || u1 != pRGB[1])) {
        pRGB[0] = u0;
        pRGB[1] = u1;
        IDIB_FlushPalette(self->m_fontBmp);
    }

    AEERasterOp rop = AEE_RO_COPY;
    if (dwFlags & IDF_TEXT_TRANSPARENT) {
        rop = AEE_RO_TRANSPARENT;
    }

    uint32 yOffset = 0;
    uint32 yHeight = self->m_ascent + self->m_descent;
    int xMin, xMax, yMin, yMax;
    xMin = prcClip->x;
    yMin = prcClip->y;
    xMax = xMin + prcClip->dx;
    yMax = yMin + prcClip->dy;
    // Clip bottom of line
    if (y + yHeight > yMax) {
        yHeight = yMax - y;
    }
    // Clip top of line
    if (y < yMin) {
        yOffset = yMin - y;
        yHeight -= yOffset;
        y = yMin;
    }
    if (yHeight <= 0) {
        return SUCCESS; // don't call BltIn() with negative height (just to be nice)
    }
    int ii;
    // Draw characters
    //
    // Invariants:
    // ii = array index of next character
    // x = x coordinate of next character
    // skip chars clipped on left
    for (ii = 0; ii < nChars; ++ii) {
        FontChar *fc = CFontJP_FindCharAECHAR(self, pcText[ii]);
        int widChar = fc->dwidthX;
        x += widChar;
        if (x > xMin) {
            x -= widChar;
            break;
        }
    }
    for (; ii < nChars && x < xMax; ++ii) {
        uint32 xChar, yChar;
        FontChar *fc = CFontJP_FindCharAECHAR(self, pcText[ii]);
        if (fc) {
            int widChar = fc->bboxWidth;
            CFontJP_LocateChar(self, fc, &xChar, &yChar);
            // Clip left side of character
            if (x < xMin) {
                xChar += xMin - x;
                widChar -= xMin - x;
                x = xMin;
            }
            // Clip right side of character
            if (x + widChar > xMax) {
                widChar = xMax - x;
            }
            int nResult = IBitmap_BltIn(pDst, x, y, widChar, yHeight,
                                        (IBitmap *) self->m_fontBmp, xChar, yChar + yOffset, rop);
            if (nResult != SUCCESS)
                return nResult;
            x += fc->dwidthX;
        } else {
            x += self->m_bboxWidth;
        }
    }
    return SUCCESS;
}


static int
CFontJP_GetInfo(IFont *pMe, AEEFontInfo *pInfo, int nSize) {
    CFont *self = (CFont *) pMe;

    if (!self->m_fontBmp) {
        int status = CFontJP_InitFont(self);
        if (status != AEE_SUCCESS) {
            return status;
        }
    }

    if (nSize != sizeof(AEEFontInfo))
        return EUNSUPPORTED;

    if (pInfo) {
        pInfo->nAscent = self->m_ascent;
        pInfo->nDescent = self->m_descent;
    }
    return SUCCESS;
}


static int
CFontJP_MeasureText(IFont *pMe, const AECHAR *pcText, int nChars, int nMaxWidth, int *pnCharFits,
                    int *pnPixels) {
    CFont *self = (CFont *) pMe;

    if (!self->m_fontBmp) {
        int status = CFontJP_InitFont(self);
        if (status != AEE_SUCCESS) {
            return status;
        }
    }

    int x = 0;
    int charFits = INT_MIN;
    for (int i = 0; i < nChars; i++) {
        FontChar *fc = CFontJP_FindCharAECHAR(self, pcText[i]);
        if (fc) {
            x += fc->dwidthX;
        } else {
            x += self->m_bboxWidth;
        }
        if (charFits == INT_MIN && x > nMaxWidth) {
            charFits = i - 1;
        }
    }
    if (charFits == INT_MIN) {
        charFits = nChars;
    }
    *pnPixels = x;
    *pnCharFits = charFits;
    return SUCCESS;
}

int CFontJP_New(IShell *ps, AEECLSID ClsId, void **ppObj) {
    CFont *pNew;

    *ppObj = NULL;

    pNew = (CFont *) AEE_NewClassEx(
            (IBaseVtbl *) &gKanjiFontFuncs,
            sizeof(CFont), TRUE);
    if (!pNew) {
        return ENOMEMORY;
    } else {
        pNew->m_pIShell = ps;
        pNew->m_nRefs = 1;
        pNew->m_fontBmp = NULL;

        *ppObj = pNew;
        return AEE_SUCCESS;
    }
}