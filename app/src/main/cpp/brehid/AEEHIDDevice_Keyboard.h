#ifndef AEEHIDDEVICE_KEYBOARD_H 
#define AEEHIDDEVICE_KEYBOARD_H

//==============================================================================
// FILE:         AEEHIDDevice_Keyboard.h
//
// SERVICES:     Human Interface Device (HID) Device Unique IDs for keyboards
//
// DESCRIPTION:  UIDs for keyboards.
//
//        Copyright (c) 2008 QUALCOMM Incorporated.
//        All Rights Reserved.
//        Qualcomm Confidential and Proprietary
//============================================================================*/

//==============================================================================
//                                 Constants
//==============================================================================

/* Device class Macros */
#define   AEEUID_HID_Keyboard_Device    0x0106c3fc

//Keyboard Button IDs
//These IDs come from the USB HID keyboard usage table, more infomation can be found at
//usb.org
#define   AEEHID_KeyboardID_A                       4
#define   AEEHID_KeyboardID_B                       5
#define   AEEHID_KeyboardID_C                       6
#define   AEEHID_KeyboardID_D                       7
#define   AEEHID_KeyboardID_E                       8
#define   AEEHID_KeyboardID_F                       9
#define   AEEHID_KeyboardID_G                       10
#define   AEEHID_KeyboardID_H                       11
#define   AEEHID_KeyboardID_I                       12
#define   AEEHID_KeyboardID_J                       13
#define   AEEHID_KeyboardID_K                       14
#define   AEEHID_KeyboardID_L                       15
#define   AEEHID_KeyboardID_M                       16
#define   AEEHID_KeyboardID_N                       17
#define   AEEHID_KeyboardID_O                       18
#define   AEEHID_KeyboardID_P                       19
#define   AEEHID_KeyboardID_Q                       20
#define   AEEHID_KeyboardID_R                       21
#define   AEEHID_KeyboardID_S                       22
#define   AEEHID_KeyboardID_T                       23
#define   AEEHID_KeyboardID_U                       24
#define   AEEHID_KeyboardID_V                       25
#define   AEEHID_KeyboardID_W                       26
#define   AEEHID_KeyboardID_X                       27
#define   AEEHID_KeyboardID_Y                       28
#define   AEEHID_KeyboardID_Z                       29
#define   AEEHID_KeyboardID_1                       30
#define   AEEHID_KeyboardID_2                       31
#define   AEEHID_KeyboardID_3                       32
#define   AEEHID_KeyboardID_4                       33
#define   AEEHID_KeyboardID_5                       34
#define   AEEHID_KeyboardID_6                       35
#define   AEEHID_KeyboardID_7                       36
#define   AEEHID_KeyboardID_8                       37
#define   AEEHID_KeyboardID_9                       38
#define   AEEHID_KeyboardID_0                       39
#define   AEEHID_KeyboardID_Enter                   40
#define   AEEHID_KeyboardID_Escape                  41
#define   AEEHID_KeyboardID_Backspace               42
#define   AEEHID_KeyboardID_Tab                     43
#define   AEEHID_KeyboardID_Spacebar                44
#define   AEEHID_KeyboardID_Minus                   45
#define   AEEHID_KeyboardID_Equal                   46
#define   AEEHID_KeyboardID_LeftBracket             47
#define   AEEHID_KeyboardID_RightBracket            48
#define   AEEHID_KeyboardID_BackSlash               49
#define   AEEHID_KeyboardID_NonUS_Pound             50
#define   AEEHID_KeyboardID_Semicolon               51
#define   AEEHID_KeyboardID_Quote                   52
#define   AEEHID_KeyboardID_Grave                   53
#define   AEEHID_KeyboardID_Comma                   54
#define   AEEHID_KeyboardID_Period                  55
#define   AEEHID_KeyboardID_Slash                   56
#define   AEEHID_KeyboardID_CapsLock                57
#define   AEEHID_KeyboardID_F1                      58
#define   AEEHID_KeyboardID_F2                      59
#define   AEEHID_KeyboardID_F3                      60
#define   AEEHID_KeyboardID_F4                      61
#define   AEEHID_KeyboardID_F5                      62
#define   AEEHID_KeyboardID_F6                      63
#define   AEEHID_KeyboardID_F7                      64
#define   AEEHID_KeyboardID_F8                      65
#define   AEEHID_KeyboardID_F9                      66
#define   AEEHID_KeyboardID_F10                     67
#define   AEEHID_KeyboardID_F11                     68
#define   AEEHID_KeyboardID_F12                     69
#define   AEEHID_KeyboardID_PrintScreen             70
#define   AEEHID_KeyboardID_ScrollLock              71
#define   AEEHID_KeyboardID_Pause                   72
#define   AEEHID_KeyboardID_Insert                  73
#define   AEEHID_KeyboardID_Home                    74
#define   AEEHID_KeyboardID_PageUp                  75
#define   AEEHID_KeyboardID_Delete                  76
#define   AEEHID_KeyboardID_End                     77
#define   AEEHID_KeyboardID_PageDown                78
#define   AEEHID_KeyboardID_RightArrow              79
#define   AEEHID_KeyboardID_LeftArrow               80
#define   AEEHID_KeyboardID_DownArrow               81
#define   AEEHID_KeyboardID_UpArrow                 82
#define   AEEHID_KeyboardID_NumLock                 83
#define   AEEHID_KeyboardID_Keypad_Divide           84
#define   AEEHID_KeyboardID_Keypad_Multiply         85
#define   AEEHID_KeyboardID_Keypad_Subtract         86
#define   AEEHID_KeyboardID_Keypad_Add              87
#define   AEEHID_KeyboardID_Keypad_Enter            88
#define   AEEHID_KeyboardID_Keypad_1                89
#define   AEEHID_KeyboardID_Keypad_2                90
#define   AEEHID_KeyboardID_Keypad_3                91
#define   AEEHID_KeyboardID_Keypad_4                92
#define   AEEHID_KeyboardID_Keypad_5                93
#define   AEEHID_KeyboardID_Keypad_6                94
#define   AEEHID_KeyboardID_Keypad_7                95
#define   AEEHID_KeyboardID_Keypad_8                96
#define   AEEHID_KeyboardID_Keypad_9                97
#define   AEEHID_KeyboardID_Keypad_0                98
#define   AEEHID_KeyboardID_Keypad_DecimalPoint     99
#define   AEEHID_KeyboardID_NonUS_Backslash         100
#define   AEEHID_KeyboardID_Application             101
#define   AEEHID_KeyboardID_Power                   102
#define   AEEHID_KeyboardID_Keypad_Equal            103
#define   AEEHID_KeyboardID_F13                     104
#define   AEEHID_KeyboardID_F14                     105
#define   AEEHID_KeyboardID_F15                     106
#define   AEEHID_KeyboardID_F16                     107
#define   AEEHID_KeyboardID_F17                     108
#define   AEEHID_KeyboardID_F18                     109
#define   AEEHID_KeyboardID_F19                     110
#define   AEEHID_KeyboardID_F20                     111
#define   AEEHID_KeyboardID_F21                     112
#define   AEEHID_KeyboardID_F22                     113
#define   AEEHID_KeyboardID_F23                     114
#define   AEEHID_KeyboardID_F24                     115
#define   AEEHID_KeyboardID_Execute                 116
#define   AEEHID_KeyboardID_Help                    117
#define   AEEHID_KeyboardID_Menu                    118
#define   AEEHID_KeyboardID_Select                  119
#define   AEEHID_KeyboardID_Stop                    120
#define   AEEHID_KeyboardID_Again                   121
#define   AEEHID_KeyboardID_Undo                    122
#define   AEEHID_KeyboardID_Cut                     123
#define   AEEHID_KeyboardID_Copy                    124
#define   AEEHID_KeyboardID_Paste                   125
#define   AEEHID_KeyboardID_Find                    126
#define   AEEHID_KeyboardID_Mute                    127
#define   AEEHID_KeyboardID_VolumeUp                128
#define   AEEHID_KeyboardID_VolumeDown              129
#define   AEEHID_KeyboardID_Locking_CapsLock        130
#define   AEEHID_KeyboardID_Locking_NumLock         131
#define   AEEHID_KeyboardID_Locking_ScrollLock      132
#define   AEEHID_KeyboardID_Keypad_Comma            133
#define   AEEHID_KeyboardID_Keypad_EqualSign        134
#define   AEEHID_KeyboardID_International1          135
#define   AEEHID_KeyboardID_International2          136     
#define   AEEHID_KeyboardID_International3          137
#define   AEEHID_KeyboardID_International4          138
#define   AEEHID_KeyboardID_International5          139
#define   AEEHID_KeyboardID_International6          140
#define   AEEHID_KeyboardID_International7          141
#define   AEEHID_KeyboardID_International8          142
#define   AEEHID_KeyboardID_International9          143
#define   AEEHID_KeyboardID_LANG1                   144
#define   AEEHID_KeyboardID_LANG2                   145
#define   AEEHID_KeyboardID_LANG3                   146
#define   AEEHID_KeyboardID_LANG4                   147
#define   AEEHID_KeyboardID_LANG5                   148
#define   AEEHID_KeyboardID_LANG6                   149
#define   AEEHID_KeyboardID_LANG7                   150
#define   AEEHID_KeyboardID_LANG8                   151
#define   AEEHID_KeyboardID_LANG9                   152
#define   AEEHID_KeyboardID_AltErase                153
#define   AEEHID_KeyboardID_SysReq                  154
#define   AEEHID_KeyboardID_Cancel                  155       
#define   AEEHID_KeyboardID_Clear                   156
#define   AEEHID_KeyboardID_Prior                   157
#define   AEEHID_KeyboardID_Return                  158
#define   AEEHID_KeyboardID_Separator               159
#define   AEEHID_KeyboardID_Out                     160
#define   AEEHID_KeyboardID_Oper                    161
#define   AEEHID_KeyboardID_ClearAgain              162
#define   AEEHID_KeyboardID_CrSel                   163
#define   AEEHID_KeyboardID_ExSel                   164
#define   AEEHID_KeyboardID_Keypad_00               176
#define   AEEHID_KeyboardID_Keypad_000              177
#define   AEEHID_KeyboardID_ThousandsSeparator      178
#define   AEEHID_KeyboardID_DecimalSeparator        179
#define   AEEHID_KeyboardID_CurrencyUnit            180
#define   AEEHID_KeyboardID_CurrencySubunit         181
#define   AEEHID_KeyboardID_Keypad_LeftParentheses  182
#define   AEEHID_KeyboardID_Keypad_RightParentheses 183
#define   AEEHID_KeyboardID_Keypad_LeftBrace        184
#define   AEEHID_KeyboardID_Keypad_RightBrace       185
#define   AEEHID_KeyboardID_Keypad_Tab              186
#define   AEEHID_KeyboardID_Keypad_Backspace        187
#define   AEEHID_KeyboardID_Keypad_A                188
#define   AEEHID_KeyboardID_Keypad_B                189
#define   AEEHID_KeyboardID_Keypad_C                190
#define   AEEHID_KeyboardID_Keypad_D                191
#define   AEEHID_KeyboardID_Keypad_E                192
#define   AEEHID_KeyboardID_Keypad_F                193
#define   AEEHID_KeyboardID_Keypad_XOR              194
#define   AEEHID_KeyboardID_Keypad_Caret            195
#define   AEEHID_KeyboardID_Keypad_Percent          196
#define   AEEHID_KeyboardID_Keypad_LessThan         197
#define   AEEHID_KeyboardID_Keypad_GreaterThan      198
#define   AEEHID_KeyboardID_Keypad_Ampersand        199
#define   AEEHID_KeyboardID_Keypad_DoubleAmpersand  200
#define   AEEHID_KeyboardID_Keypad_Pipe             201
#define   AEEHID_KeyboardID_Keypad_DoublePipe       202
#define   AEEHID_KeyboardID_Keypad_Colon            203
#define   AEEHID_KeyboardID_Keypad_Pound            204
#define   AEEHID_KeyboardID_Keypad_Space            205
#define   AEEHID_KeyboardID_Keypad_At               206
#define   AEEHID_KeyboardID_Keypad_Exclamation      207
#define   AEEHID_KeyboardID_Keypad_MemStore         208
#define   AEEHID_KeyboardID_Keypad_MemRecall        209
#define   AEEHID_KeyboardID_Keypad_MemClear         210
#define   AEEHID_KeyboardID_Keypad_MemAdd           211
#define   AEEHID_KeyboardID_Keypad_MemSubtract      212
#define   AEEHID_KeyboardID_Keypad_MemMultiply      213
#define   AEEHID_KeyboardID_Keypad_MemDivide        214
#define   AEEHID_KeyboardID_Keypad_PlusMinus        215
#define   AEEHID_KeyboardID_Keypad_Clear            216
#define   AEEHID_KeyboardID_Keypad_ClearEntry       217
#define   AEEHID_KeyboardID_Keypad_Binary           218
#define   AEEHID_KeyboardID_Keypad_Octal            219
#define   AEEHID_KeyboardID_Keypad_Decimal          220
#define   AEEHID_KeyboardID_Keypad_Hexadecimal      221
#define   AEEHID_KeyboardID_LeftControl             224
#define   AEEHID_KeyboardID_LeftShift               225
#define   AEEHID_KeyboardID_LeftAlt                 226
#define   AEEHID_KeyboardID_LeftGUI                 227
#define   AEEHID_KeyboardID_RightControl            228
#define   AEEHID_KeyboardID_RightShift              229
#define   AEEHID_KeyboardID_RightAlt                230
#define   AEEHID_KeyboardID_RightGUI                231

/*==============================================================================
DATA STRUCTURES DOCUMENTATION
================================================================================

AEEUID_HID_Keyboard_Device

Description: 
   This UID indicates that this device is a keyboard.

See Also:
   AEEHIDDeviceInfo
   IHID_GetConnectedDevices()

================================================================================

AEEHID_KeyboardID*

Description: 
   These ID are from the USB HID usage tables specification and defines 
   constants that can be used to determine which key was pressed/released. 

See Also:
   AEEHIDButtonInfo

================================================================================
*/

#endif    // AEEHIDDEVICE_KEYBOARD_H

