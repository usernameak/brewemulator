#include "AEE_OEMFile.h"
#include "AEEStdLib.h"

const AEEConstFile gAEESMSDEBUGSET_MIF = {"fs:/SMSDebugSet.invalid",FALSE,0,0,0,(byte *)NULL};
const AEEConstFile gAEETELEPHONYDEBUGSET_MIF = {"fs:/TelDebugSet.invalid",FALSE,0,0,0,(byte *)NULL};
const AEEConstFile gAEENETWORKDEBUGSET_MIF = {"fs:/NetDebugSet.invalid",FALSE,0,0,0,(byte *)NULL};
const AEEConstFile gAEEDNSDEBUGSET_MIF = {"fs:/DNSDebugSet.invalid",FALSE,0,0,0,(byte *)NULL};
const AEEConstFile gAEEWEBDEBUGSET_MIF = {"fs:/WebDebugSet.invalid",FALSE,0,0,0,(byte *)NULL};
const AEEConstFile gAEEQOSDEBUGSET_MIF = {"fs:/QoSDebugSet.invalid",FALSE,0,0,0,(byte *)NULL};
const AEEConstFile gAEESOCKETDEBUGSET_MIF = {"fs:/SckDebugSet.invalid",FALSE,0,0,0,(byte *)NULL};
