cmake_minimum_required(VERSION 3.17)

project(ktme C)

set(CMAKE_C_STANDARD 99)

option(KTME_ENABLE_APPS "Enable various KTME apps and examples" OFF)

add_subdirectory(ktme)
if (KTME_ENABLE_APPS)
    add_subdirectory(ktmeTestApp)
endif()
